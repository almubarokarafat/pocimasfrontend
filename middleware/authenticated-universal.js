export default async ({ store, redirect, app }) => {
  // console.log('store state user:', store.state.user)
  // console.log('/n')

  const isLogin = app.$cookies.get('g_isLogin') === 'true'
  // console.log('isLogin:', isLogin)
  // console.log('/n')

  const pocimasAccKey = app.$cookies.get('g_accKey') || ''
  // console.log('pocimasAccKey:', pocimasAccKey)
  // console.log('/n')

  const pocimasRefKey = app.$cookies.get('g_refKey') || ''
  // console.log('pocimasRefKey:', pocimasRefKey)
  // console.log('/n')

  const pocimasUserUUID = app.$cookies.get('g_userUUID') || ''
  // console.log('pocimasUserUUID:', pocimasUserUUID)
  // console.log('/n')

  if (pocimasRefKey === '') {
    store.dispatch('user/doLogout')
  }

  // If the user is authenticated
  if (isLogin && pocimasAccKey !== '') {
    store.commit('user/setState', {
      isLogin: isLogin,
      accKey: pocimasAccKey,
      refKey: pocimasRefKey,
      uuid: pocimasUserUUID
    })
    // refresh token if access token empty
  } else if (isLogin && pocimasAccKey === '') {
    // refill isLogin & refKey state from cookie
    await store.commit('user/setState', {
      isLogin: isLogin,
      refKey: pocimasRefKey
    })
    // refresh token
    await store.dispatch('user/refreshAuth')
  } else {
    return redirect('/')
  }
}
