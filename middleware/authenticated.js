export default async ({ store, redirect, app, route }) => {
  // console.log('store state user:', store.state.user)
  // console.log('/n')

  const isLogin = app.$cookies.get('g_isLogin') === 'true'
  // console.log('isLogin:', isLogin)
  // console.log('/n')

  const pocimasAccKey = app.$cookies.get('g_accKey') || ''
  // console.log('pocimasAccKey:', pocimasAccKey)
  // console.log('/n')

  const pocimasRefKey = app.$cookies.get('g_refKey') || ''
  // console.log('pocimasRefKey:', pocimasRefKey)
  // console.log('/n')

  const pocimasUserUUID = app.$cookies.get('g_userUUID') || ''

  if (pocimasRefKey === '') {
    store.dispatch('user/doLogout')
  }

  // If the user is authenticated
  if (isLogin && pocimasAccKey !== '') {
    store.commit('user/setLogin', isLogin)
    store.commit('user/setAuthToken', {
      accKey: pocimasAccKey,
      refKey: pocimasRefKey
    })
    store.commit('user/setUUID', pocimasUserUUID)
    // refresh token if access token empty
  } else if (isLogin && pocimasAccKey === '') {
    // refill isLogin & refKey state from cookie
    store.commit('user/setLogin', isLogin)
    store.commit('user/setAuthToken', {
      refKey: pocimasRefKey
    })
    // refresh token
    await store.dispatch('user/refreshAuth')
  } else {
    return redirect('/')
  }
}
