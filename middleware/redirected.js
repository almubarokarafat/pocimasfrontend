export default function({ store, route, redirect, app }) {
  const isLogin = app.$cookies.get('g_isLogin') === 'true'
  // const pocimasAccKey = app.$cookies.get('g_accKey') || ''
  // // console.log('pocimasAccKey:', pocimasAccKey)
  // const pocimasRefKey = app.$cookies.get('g_refKey') || ''
  // const pocimasUserUUID = app.$cookies.get('g_userUUID') || ''

  // console.warn('redirect isLogin:', isLogin)

  // If the user is not authenticated
  if (isLogin && (route.path === '/login' || route.path === '/')) {
    // console.warn('redirect login / root:', isLogin)
    // store.commit('user/setState', {
    //   isLogin: isLogin,
    //   accKey: pocimasAccKey,
    //   refKey: pocimasRefKey,
    //   uuid: pocimasUserUUID
    // })
    return redirect('/home')
  } else {
    return true
  }
}
