const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const pkg = require('./package')
require('dotenv').config()

module.exports = {
  /*
  ** website build mode
  */
  mode: 'spa',
  /*
  ** npm run generate options
  */

  env: {
    pocimas_app_title: process.env.pocimas_APP_TITLE,
    pocimas_api_url: process.env.pocimas_API_URL,
    pocimas_client_id: process.env.pocimas_CLIENT_ID,
    pocimas_basic_auth: process.env.pocimas_BASIC_AUTH,
    pocimas_expire_refresh: process.env.pocimas_EXPIRE_REFRESH,
    pocimas_mixpanel_token: process.env.pocimas_MIXPANEL_TOKEN,
    pocimas_buyer_app_url: process.env.pocimas_BUYER_APP_URL,
    pocimas_sheetdb_auth: process.env.pocimas_SHEETDB_AUTH,
    pocimas_sheetdb_url: process.env.pocimas_SHEETDB_URL,
    pocimas_app_google_maps_api_key:
      process.env.pocimas_APP_GOOGLE_MAPS_API_KEY,
    pocimas_freshchat_token: process.env.pocimas_FRESHCHAT_TOKEN
  },
  manifest: {
    lang: 'id',
    name: 'POCIMAS',
    short_name:
      'Pasarind Open Channel Inisiative and Mudahind Automation System',
    theme_color: '#00bcb8',
    display: 'standalone'
  },
  generate: {
    dir: 'build'
  },

  /*
  ** router options
  */
  router: {
    // To disable prefetching, uncomment the line
    prefetchLinks: true,
    // Activate prefetched class (default: false)
    // Used to display the check mark next to the prefetched link
    linkPrefetchedClass: 'link-prefetched',
    extendRoutes(routes) {
      routes.push({
        name: 'reset_password',
        path: '/user/reset/:uid/:timestamp/:hash',
        component: '~/pages/forgot-password/reset/index.vue',
        meta: { guest: true }
      })
    }
  },

  /*
  ** Headers of the page
  */
  head: {
    title: 'Pocimas',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'author', content: 'Pocimas Tech' }
    ],
    noscript: [
      { innerHTML: 'Pocimas website require JavaScript.', body: true }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    link: [
      { rel: 'preconnect', href: 'https://onesignal.com' },
      { rel: 'dns-prefetch', href: 'https://onesignal.com' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: 'aqua',
    failedColor: 'red',
    height: '4px'
  },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl',
    '~/assets/style/transition.css',
    '~/assets/style/material-icons.css',
    '~/assets/style/roboto.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/axios',
    '@/plugins/vuetify',
    // { src: '@/plugins/mixpanel.js', ssr: false },
    { src: '~/plugins/google-maps', ssr: false },
    { src: '~/plugins/vuex-cache.js', ssr: false },
    // { src: '~/plugins/nuxt-inject.js', ssr: false },
    { src: '@/plugins/persistedstate', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/axios', // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/onesignal',
    '@nuxtjs/pwa',
    ['cookie-universal-nuxt', { parseJSON: false }],
    '@nuxtjs/robots',
    'nuxt-purgecss',
    '@nuxtjs/universal-storage'
  ],

  /*
  ** setting enable nuxt universal storage
  */
  storage: {
    vuex: false, // boolean or {namespace}
    localStorage: { prefix: 'pocimas_' }, // boolean or {prefix }
    cookie: false, // boolean or {prefix, options }
    initialState: false, // Object {}
    ignoreExceptions: false //
  },

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.pocimas_API_URL,
    retry: { retries: 3 },
    proxyHeaders: false,
    credentials: false
  },

  robots: {
    UserAgent: '*',
    Disallow: ''
  },

  oneSignal: {
    init: {
      appId: process.env.pocimas_ONESIGNAL_APP_ID,
      allowLocalhostAsSecureOrigin: true
    }
  },
  // Workbox options
  workbox: {
    workboxURL:
      'https://cdn.jsdelivr.net/npm/workbox-cdn/workbox/workbox-sw.js',
    cleanupOutdatedCaches: true,
    globPatterns: [
      '**/*.{css,png,svg,json,woff2}',
      '**/img/*',
      '**/icons/*',
      '**/fonts/*'
    ],
    offlineAssets: ['/icon.png', '/favicon.ico'],
    runtimeCaching: [
      {
        // Should be a regex string. Compiles into new RegExp('https://my-cdn.com/.*')
        urlPattern: 'https://cdn.onesignal.com/sdks/*',
        strategyOptions: {
          cacheName: 'onesignal-cache',
          cacheExpiration: {
            maxEntries: 10,
            // maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
            maxAgeSeconds: 604800
          }
        }
      },
      {
        // Should be a regex string. Compiles into new RegExp('https://my-cdn.com/.*')
        urlPattern: 'https://onesignal.com/api/v1/apps/*/icon',
        strategyOptions: {
          cacheName: 'onesignal-icon',
          cacheExpiration: {
            maxEntries: 10,
            // maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
            maxAgeSeconds: 604800
          }
        }
      },
      {
        // Should be a regex string. Compiles into new RegExp('https://my-cdn.com/.*')
        urlPattern: 'https://onesignal.com/api/v1/sync/*/web?callback=__jp0',
        strategyOptions: {
          cacheName: 'onesignal-api',
          cacheExpiration: {
            maxEntries: 10,
            // maxAgeSeconds: 30 * 24 * 60 * 60 // 30 Days
            maxAgeSeconds: 604800
          }
        }
      }
    ]
  },

  // render option
  render: {
    bundleRenderer: {
      shouldPrefetch: (file, type) => ['script', 'style', 'font'].includes(type)
    }
  },
  /*
  ** Build configuration
  */
  build: {
    optimizeCSS: true,
    optimization: {
      splitChunks: {
        chunks: 'all',
        minSize: 10000,
        maxSize: 200000
      }
    },
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      config.resolve.alias.vue = 'vue/dist/vue.common'
    }
  },
  /**
   * PurgeCSS
   */
  purgeCSS: {
    mode: 'webpack',
    enabled: ({ isDev, isClient }) => !isDev && isClient, // or `false` when in dev/debug mode
    paths: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js'
    ],
    styleExtensions: ['.css'],
    whitelist: ['body', 'html', 'nuxt-progress'],
    extractors: [
      {
        extractor: class {
          static extract(content) {
            return content.match(/[A-z0-9-:\\/]+/g)
          }
        },
        extensions: ['html', 'vue', 'js']
      }
    ]
  },
  generate: {
    dir: 'public'
  }
}
