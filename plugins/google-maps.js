import Vue from 'vue'

import * as VueGoogleMaps from '~/node_modules/vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.pocimas_app_google_maps_api_key,
    libraries: 'places'
  }
})
