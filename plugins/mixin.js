import Vue from 'vue'

/**
 * Fetch retry request
 */
async function fetchRetry({
  actionDispatch,
  param,
  useCache,
  retry,
  cacheTime
}) {
  let request = false

  // Define Vuex Store
  const store = window.$nuxt.$store

  // Do the request with custom retry
  let isMaintenance = false
  let isDone = false
  const maxRetry = typeof retry !== 'undefined' ? parseInt(retry) : 3
  let processCounter = 1
  while (!isDone) {
    // Define Cache Time if use Cache & Cache Time not defined
    if (
      typeof useCache !== 'undefined' &&
      useCache &&
      typeof cacheTime === 'undefined'
    ) {
      cacheTime = 1 * 5 * 1000
    }

    request =
      typeof useCache !== 'undefined' &&
      useCache &&
      typeof store.cache !== 'undefined'
        ? await store.cache.dispatch(actionDispatch, param, cacheTime)
        : await store.dispatch(actionDispatch, param)

    if (typeof request === 'object') {
      if (request.hasOwnProperty('data')) {
        isDone = true
      } else if (request.hasOwnProperty('errors')) {
        if (request.errors[0].status === 401) {
          // Do Refresh Token
          await store.dispatch('user/refreshAuth')
          // Re-set the token
          param.token = store.state.user.accKey
        } else if (
          [403, 404, 500, 503].indexOf(request.errors[0].status) >= 0
        ) {
          if (request.errors[0].status === 503) {
            isMaintenance = true
          }
          isDone = true
        }
      }
    } else if (typeof request === 'string') {
      // maintenance
      isMaintenance = true
      isDone = true
    }

    if (processCounter >= maxRetry) {
      isDone = true
    }
    processCounter++
  }

  if (isMaintenance) {
    const redirUrl = `/maintenance?prev=${store.$router.app._route.fullPath}`
    store.$router.push(redirUrl)
  }

  return request
}

// Register as global
Vue.mixin({
  methods: {
    fetchRetry
  }
})
