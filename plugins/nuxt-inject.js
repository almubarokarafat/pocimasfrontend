export default ({ store, app }, inject) => {
  inject('printString', (string = 'Hola') => {
    console.warn('Params value: ', string)
    return string
  })
}
