import createPersistedState from 'vuex-persistedstate'

// state tha need to show immediately when refresh
// walletBalance: state => state.user.wallet_balance,
// storeName: state => state.mart.default.name,
// storeCode: state => state.mart.default.coverID,
// userPhone: state => state.profile.default.phone,
// storeAvatar: state => state.profile.default.photo_url,
// userName: state => state.profile.default.address.given_name,

// user.isLogin
// user.accKey
// user.refKey
// user.uuid
// user.roles
// user.email
// user.wallet_balance

export default ({ store, req, isDev }) => {
  createPersistedState({
    key: 'pocimas',
    paths: [
      'user.isLogin',
      'user.accKey',
      'user.refKey',
      'user.uid',
      'user.uuid',
      'user.uid',
      'user.roles',
      'user.email',
      'user.wallet_balance',
      'mart.default.name',
      'mart.default.coverID',
      'profile.default.phone',
      'profile.default.photo_url',
      'profile.default.given_name',
      'mart.default.id',
      // 'product.list'
      'order.placeID',
      'user.uid'
    ]
  })(store)
}
