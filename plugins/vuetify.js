import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// import colors from 'vuetify/es5/util/colors'
const LRU = require('lru-cache')
const themeCache = new LRU({
  max: 10,
  maxAge: 1000 * 60 * 60 // 1 hour
})

Vue.use(Vuetify, {
  theme: {
    // primary: '#00bcb8', // a color that is not in the material colors palette
    primary: '#1F768A',
    // secondary: '#fe4544',
    secondary: '#FFAF20 ',
    accent: '#969696',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107',
    black38: '#383838',
    black46: '#383838',
    coral: '#fe4544',
    turquoise: '#00bcb8',
    warmGrey: '#969696',
    offWhite: '#f5f5f5',
    lightBlueGrey: '#def2ee',
    lighterGrey: '#7c7c7c',
    orange: '#fc4827'
  },
  themeCache,
  minifyTheme: function(css) {
    return process.env.NODE_ENV === 'production'
      ? css.replace(/[\s|\r\n|\r|\n]/g, '')
      : css
  }
})
