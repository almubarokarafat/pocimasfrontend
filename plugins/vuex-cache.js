import createVuexCache from 'vuex-cache'

export default ({ store }) => {
  const options = {
    // cache timeout global for 30 seconds
    timeout: 1 * 30 * 1000 // in milliseconds.
  }

  const setupVuexCache = createVuexCache(options)

  window.onNuxtReady(() => setupVuexCache(store))
}
