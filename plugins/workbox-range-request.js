const workbox = window.$workbox
if (workbox) {
  workbox.routing.registerRoute(
    /.*\.(mp4|webm)/,
    workbox.strategies.cacheFirst({
      plugins: [new workbox.rangeRequests.Plugin()]
    }),
    'GET'
  )
}
