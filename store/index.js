/* 
* This is the root of store which are other files as vuex store module 
* define related state unique name in each files
*
* https://nuxtjs.org/guide/vuex-store#modules-mode 
*/

// const env = process.env

// state defined as function
export const state = () => ({
  counter: 0,
  pageTitle: '',
  daftarJual: [],
  daftarBeli: []
})

// mutations defined as object
export const mutations = {
  increment(state, params) {
    state.counter = state.counter + params
  },
  setPageTitle(state, params) {
    state.pageTitle = params
  }
}

// sample root vuex getter
// export const getters = {
//   pageTitle(state) {
//     return state.pageTitle
//   }
// }

// this is root of store, mutations defined must unique
export const actions = {
  async nuxtServerInit({ dispatch }, { request, app }) {
    const isLogin = (await app.$cookies.get('g_isLogin')) === 'true'
    console.log('nuxtServerInit isLogin:', isLogin)
    // console.log('/n')

    const pocimasAccKey = (await app.$cookies.get('g_accKey')) || ''
    console.log('nuxtServerInit pocimasAccKey:', pocimasAccKey)
    // console.log('/n')

    const pocimasRefKey = (await app.$cookies.get('g_refKey')) || ''
    console.log('nuxtServerInit pocimasRefKey:', pocimasRefKey)
    // console.log('/n')

    const pocimasUserUUID = (await app.$cookies.get('g_userUUID')) || ''
    console.log('nuxtServerInit pocimasUserUUID:', pocimasUserUUID)
    // console.log('/n')

    // if (
    //   isLogin &&
    //   pocimasAccKey !== '' &&
    //   pocimasRefKey !== '' &&
    //   pocimasUserUUID !== ''
    // ) {
    //   const authCookie = {
    //     isLogin: isLogin,
    //     accKey: pocimasAccKey,
    //     refKey: pocimasRefKey,
    //     uuid: pocimasUserUUID
    //   }
    //   await dispatch('user/setAuthToken', authCookie, { root: true })
    //   await dispatch('setupAuthCookies')
    // }
    // here my context is updated :)
  },
  setIncrement({ commit }, intParams) {
    commit('increment', intParams)
  }
}
