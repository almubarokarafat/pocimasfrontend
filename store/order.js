export const state = () => ({
  listOrderNew: [],
  listOrderPackaged: [],
  listOrderSending: [],
  listOrderCanceled: [],
  listOrderReceived: [],
  newLoading: false,
  packagedLoading: false,
  sendingLoading: false,
  canceledLoading: false,
  receivedLoading: false,
  orderNewDetail: '',
  listItem: '',
  paramID: '',
  titleProd: ''
})

const initState = state()

export const mutations = {
  resetState(state) {
    Object.assign(state, initState)
  },
  setState(state, params) {
    const keys = Object.keys(params)
    keys.forEach(key => (state[key] = params[key]))
  },
  setDefaultOrderNew(state, params) {
    state.listOrderNew = params
  },
  setDefaultOrderPackaged(state, params) {
    state.listOrderPackaged = params
  },
  setDefaultOrderSending(state, params) {
    state.listOrderSending = params
  },
  setDefaultOrderCanceled(state, params) {
    state.listOrderCanceled = params
  },
  setDefaultOrderReceived(state, params) {
    state.listOrderReceived = params
  },
  setParam(state, params) {
    state.paramID = params
  },
  setOrderNewDetail(state, params) {
    state.orderNewDetail = params
    state.listItem = params.listItem
  }
}

export const actions = {
  resetState({ commit }) {
    commit('resetState')
  },
  setParam({ commit }, resOrder) {
    commit('setParam', resOrder)
  },
  setDefaultOrderNew({ commit }, resOrder) {
    commit('setDefaultOrderNew', resOrder)
  },
  setDefaultOrderPackaged({ commit }, resOrder) {
    commit('setDefaultOrderPackaged', resOrder)
  },
  setDefaultOrderSending({ commit }, resOrder) {
    commit('setDefaultOrderSending', resOrder)
  },
  setDefaultOrderCanceled({ commit }, resOrder) {
    commit('setDefaultOrderCanceled', resOrder)
  },
  setDefaultOrderReceived({ commit }, resOrder) {
    commit('setDefaultOrderReceived', resOrder)
  },
  setOrderNewDetail({ commit }, resOrder) {
    // console.warn('resOrder', resOrder)
    commit('setOrderNewDetail', resOrder)
  },
  /**
   * Function to get list of new order
   *
   * @param {*} { dispatch, commit }
   * @param {*} { token }
   * @returns array of new order data sorted by descending placed time
   */
  getOrder({ commit }, status) {
    const loading = status === 'ready' ? 'newLoading' : `${status}Loading`
    commit('setState', {
      [loading]: true
    })
    const token = this.$cookies.get('g_refKey')
    this.$axios.setToken(token, 'Bearer')
    const url = `/order/jual/status/${status}`
    return this.$axios.$get(url)
  }
}
