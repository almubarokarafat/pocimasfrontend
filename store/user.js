import { _isUndefined } from '~/utils'

export const state = () => ({
  isLogin: false,
  changePassLoading: false,
  changePassStatus: false,
  loginLoading: false,
  isLoadingProduct: true,
  username:
    process.env.NODE_ENV === 'development' ? 'kobarganteng@gmail.com' : '',
  password: process.env.NODE_ENV === 'development' ? 'kobarpass' : '',
  accKey: '',
  refKey: '',
  uuid: '',
  accExpKey: '',
  roles: [],
  email: '',
  wallet_balance: 0,
  hasProduct: false,
  hasOrder: false,
  search: '',
  uid: '',
  fullname: '',
  noHandphone: '',
  userIdentity: '',
  dateCreated: null,
  dateChanged: null,
  userChangedProfile: true,
  saldo: 0
})

const initState = state()

// mutations defined as object
export const mutations = {
  resetState(state) {
    Object.assign(state, initState)
  },
  setState(state, params) {
    const keys = Object.keys(params)
    keys.forEach(key => (state[key] = params[key]))
  },
  setAuthToken(state, params) {
    const keys = Object.keys(params)
    keys.forEach(key => (state[key] = params[key]))
  },
  setLogin(state, param) {
    state.isLogin = param
  },
  setUID(state, usrUID) {
    state.uid = usrUID
  },
  setUUID(state, usrUUID) {
    state.uuid = usrUUID
  },
  setEmail(state, usrEmail) {
    state.email = usrEmail
  },
  setRoles(state, usrRoles) {
    state.roles = usrRoles
  },
  setDateCreated(state, dateCreated) {
    state.dateCreated = dateCreated
  },
  setDateChanged(state, dateChanged) {
    state.dateChanged = dateChanged
  },
  setHasOrder(state, param) {
    state.hasOrder = param
  },
  setHasProduct(state, param) {
    state.hasProduct = param
  },
  setStatusProfileUser(state, userChangedProfile) {
    state.userChangedProfile = userChangedProfile
  }
}
// end of mutations

export const actions = {
  // Reset user states
  resetState({ commit }) {
    commit('resetState')
  },
  setAuthToken({ commit }, params) {
    commit('setAuthToken', params)
  },
  // actions for post user login
  postLogin({ dispatch, commit, state }) {
    const data = {
      grant_type: 'password',
      email: state.username,
      password: state.password
    }
    return this.$axios.$post('users/login', data).catch(error => {
      // handle error
      if (error.response.status === 401) {
        const errMsg = error.response.data.message
          ? error.response.data.message
          : 'Username atau Kata Sandi salah'
        const alertMsg = {
          msg: errMsg,
          color: 'secondary'
        }
        dispatch('ui/showAlert', alertMsg, { root: true })
      } else {
        const alertMsg = {
          msg: 'Unknown error please contact admin'
        }
        dispatch('ui/showAlert', alertMsg, { root: true })
      }
      commit('setState', { loginLoading: false })
    })
  },
  // switch token from vue component
  refreshToken({ dispatch, state, app }) {
    const axiosOptions = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: 'Basic ' + process.env.pocimas_basic_auth
      }
    }
    const refKey =
      state.refKey !== '' ? state.refKey : app.$cookies.get('g_refKey')

    if (refKey === '' || refKey === null) {
      dispatch('doLogout')
    }
    const data = {
      grant_type: 'refresh_token',
      refresh_token: refKey
    }
    const querystring = require('querystring')
    const postData = querystring.stringify(data)
    return this.$axios
      .$post('oauth/token', postData, axiosOptions)
      .catch(error => {
        const errData = error.response.data
        // handle error
        if (!_isUndefined(errData.message)) {
          const errMsg = errData.message
            ? errData.message
            : 'Username atau Kata Sandi salah'
          const alertMsg = {
            msg: errMsg,
            color: 'secondary'
          }

          if (
            errData.error === 'invalid_request' &&
            errData.hint === 'Token has been revoked'
          ) {
            dispatch('doLogout')
          } else {
            dispatch('ui/showAlert', alertMsg, { root: true })
          }
        }
      })
  },
  // switch token from buyer
  switchToken({ dispatch }, { exRefToken }) {
    const axiosOptions = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: 'Basic ' + process.env.pocimas_basic_auth
      },
      validateStatus: status => {
        const result = status >= 200 && status <= 505
        return result
      }
    }
    const data = {
      grant_type: 'refresh_token',
      refresh_token: exRefToken
    }
    const querystring = require('querystring')
    const postData = querystring.stringify(data)
    return this.$axios
      .$post('oauth/token', postData, axiosOptions)
      .then(result => {
        // If Success
        if (
          !_isUndefined(result.status) &&
          (String(result.status) === '200' ||
            String(result.status) === '201') &&
          !_isUndefined(result.data)
        ) {
          const response = typeof result.data !== 'string' ? result.data : {}
          response.http = result.status
          // Return data.
          return response
        }

        // Other than 200 && 201 OK
        if (!_isUndefined(result.data)) {
          let response = result.data
          if (response instanceof Object) {
            response.http =
              typeof result.status !== 'undefined' ? result.status : '0'
          } else {
            const resp = {}
            resp.data = response
            resp.http =
              typeof result.status !== 'undefined' ? result.status : '0'
            response = resp
          }

          // Return data.
          return response
        }

        return result
      })
  },
  setSwitch({ commit }, params) {
    commit('setState', params)
  },
  // actions for get user UUID
  getUUID({ dispatch, commit, state }) {
    this.$axios.setToken(state.accKey, 'Bearer')
    return this.$axios.$get('jsonapi').catch(() => {
      const alertMsg = {
        msg: 'Get UUID failed'
      }
      dispatch('ui/showAlert', alertMsg, { root: true })
      // clear all related state
      commit('resetState')
    })
  },
  // actions for get user roles
  getUserUser({ dispatch }, { token, usrUUID }) {
    // this.$axios.setHeader('Content-Type', 'application/vnd.api+json')
    // this.$axios.setHeader('Accept', 'application/vnd.api+json')
    this.$axios.setToken(token, 'Bearer')
    return this.$axios
      .$get(`jsonapi/user/user/${usrUUID}?include=roles`)
      .catch(() => {
        const alertMsg = {
          msg: 'Get Roles failed'
        }
        dispatch('ui/showAlert', alertMsg, { root: true })
      })
  },
  // show unknown alert
  showUnknownAlert({ dispatch, commit }) {
    const alertMsg = {
      msg: 'Unknown response, please contact us.'
    }
    dispatch('ui/showAlert', alertMsg, { root: true })
    commit('setState', { loginLoading: false })
  },

  /**
   * Action to setup user cookies
   *
   * @param {*} { dispatch, commit, state }
   */
  setupAuthCookies({ dispatch, commit, state }, token) {
    commit('setLogin', true)

    const uRoles = []

    const pocimasExpireRefresh =
      parseInt(process.env.pocimas_expire_refresh) * 1000
    // calculate expires
    const expDate = new Date()
    expDate.setTime(expDate.getTime() + (state.accExpKey - 10) * 1000)
    const expDate2 = new Date()
    expDate2.setTime(expDate2.getTime() + pocimasExpireRefresh)

    const options = {
      path: '/',
      expires: expDate
    }
    const options2 = {
      path: '/',
      expires: expDate2
    }
    //

    const cookieList = [
      { name: 'g_isLogin', value: true, opts: options2 },
      { name: 'g_accKey', value: state.accKey, opts: options },
      { name: 'g_refKey', value: state.refKey, opts: options2 },
      { name: 'g_roles', value: state.roles, opts: options },
      {
        name: 'g_userUUID',
        // value: uuidReq.meta.links.me.meta.id,
        opts: options
      }
    ]
    this.$cookies.setAll(cookieList)
  },
  addToCookie({ commit }, params) {
    const expDate = new Date()
    const pocimasExpireRefresh =
      parseInt(process.env.pocimas_expire_refresh) * 1000
    expDate.setTime(expDate.getTime() + pocimasExpireRefresh)
    const options = {
      path: '/',
      expires: expDate
    }
    const cookieList = [
      {
        name: params.name,
        value: params.value,
        opts: options
      }
    ]
    this.$cookies.setAll(cookieList)
  },
  // actions for user logout
  async doLogout({ commit, dispatch }) {
    // reset user states
    commit('resetState')
    // reset mart states
    dispatch('mart/resetState', null, { root: true })
    // reset profile states
    dispatch('profile/resetState', null, { root: true })
    // reset order states
    dispatch('order/resetState', null, { root: true })
    // reset product states
    dispatch('product/resetState', null, { root: true })
    // remove cookies
    await this.$cookies.removeAll()
    // clear localStorage
    // await localStorage.clear()
  },

  // actions when user has product data then set hasProduct to true
  setHasProduct({ commit }, { param }) {
    commit('setHasProduct', param)
  },
  // actions when user has order data then set hasOrder to true
  setHasOrder({ commit }, { param }) {
    commit('setHasOrder', param)
  },

  /**
   * Actions for user login
   *
   * @param {*} { dispatch, commit }
   * @returns
   */
  async doLogin({ dispatch, commit }) {
    commit('setState', { loginLoading: true })

    const loginReq = await dispatch('postLogin')

    if (loginReq && loginReq.hasOwnProperty('token')) {
      const token = loginReq.token
      commit('setAuthToken', {
        accKey: token,
        refKey: loginReq.token,
        accExpKey: 10000,
        uid: loginReq.payload._id,
        saldo: loginReq.payload.saldo
      })
      // setup auth cookies
      await dispatch('setupAuthCookies', token)
    }
  },
  /**
   * Actions for refresh token
   *
   * @param {*} { dispatch, commit }
   * @returns
   */
  async refreshAuth({ dispatch, commit }) {
    //
    //
    const refreshReq = await dispatch('refreshToken')
    //
    if (
      !_isUndefined(refreshReq) &&
      refreshReq.hasOwnProperty('access_token')
    ) {
      commit('setAuthToken', {
        accKey: refreshReq.access_token,
        refKey: refreshReq.refresh_token,
        accExpKey: refreshReq.expires_in
      })
      // setup auth cookies
      await dispatch('setupAuthCookies')
      return refreshReq
    }
  },
  postUpdatePassword(
    { dispatch },
    { token, userUUID, oldPassword, newPassword }
  ) {
    this.$axios.setHeader('Content-Type', 'application/vnd.api+json', ['patch'])
    this.$axios.setToken(token, 'Bearer')

    // Build Parameter
    const postData = {
      data: {
        id: userUUID,
        type: 'user--user',
        attributes: {
          pass: {
            value: newPassword,
            existing: oldPassword
          }
        }
      }
    }
    // End Build Parameter

    return this.$axios
      .$patch(`jsonapi/user/user/${userUUID}`, JSON.stringify(postData))
      .then(result => {
        return true
      })
      .catch(error => {
        //
        // handle error
        if (error.response.status === 422) {
          // Current Password input is unmatch with the database
          const errMsg =
            error.response.data !== '' &&
            typeof error.response.data.errors[0].detail !== 'undefined'
              ? error.response.data.errors[0].detail
              : 'Current Password is invalid.'
          const alertMsg = {
            msg: errMsg
          }
          dispatch('ui/showAlert', alertMsg, { root: true })
        } else {
          const alertMsg = {
            msg: 'Unknown error please contact admin'
          }
          dispatch('ui/showAlert', alertMsg, { root: true })
        }
        return false
      })
  },
  // actions for change password process
  async doUpdatePassword(
    { dispatch, commit },
    { token, userUUID, oldPassword, newPassword }
  ) {
    commit('setState', { changePassLoading: true, changePassStatus: false })
    let changePassStatus = false
    const updateReq = await dispatch('postUpdatePassword', {
      token,
      userUUID,
      oldPassword,
      newPassword
    })

    // if update password success
    if (updateReq) {
      // then refresh the token (relogin)
      await commit('setState', {
        password: newPassword
      })
      const reLogin = await dispatch('postLogin')
      // set new token
      if (reLogin && reLogin.hasOwnProperty('access_token')) {
        commit('setAuthToken', {
          accKey: reLogin.access_token,
          refKey: reLogin.refresh_token,
          accExpKey: reLogin.expires_in
        })
        // setup auth cookies
        await dispatch('setupAuthCookies')
        changePassStatus = true
      } else {
        dispatch('showUnknownAlert')
      }
    }
    commit('setState', { changePassLoading: false, changePassStatus })

    if (changePassStatus) {
      const alertMsg = {
        msg: 'Update Password Successfull'
      }
      dispatch('ui/showAlert', alertMsg, { root: true })
    }
  }
}
// end of actions
