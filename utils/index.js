// check if variable is undefined
export const _isUndefined = a => {
  return typeof a === 'undefined'
}

// check if object is empty
// ref: https://jsperf.com/empty-object-test/19
export const _isEmptyObj = obj => {
  const r = []
  r.push(
    (() => {
      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          return false
        }
      }
    })()
  )
}

// find object match criteria from array stacks by property
// ref: https://jsperf.com/array-find-loop-native-lodash
export const _findObjByProp = (stacks, match, prop) => {
  let needle = {}

  for (let i = 0; i < stacks.length; i++) {
    if (stacks[i][prop] === match) {
      needle = stacks[i]
      break
    }
  }
  return needle
}

// get nested object
export const getNestedObject = (nestedObj, pathArr) => {
  return pathArr.reduce(
    (obj, key) => (obj && obj[key] !== 'undefined' ? obj[key] : undefined),
    nestedObj
  )
}

// function to convert text into a link text
export const convertToLinkText = text => {
  // regex to remove tag html
  const regexTag = /<(?!\/?a(?=>|\s.*>))\/?.*?>/g
  const removeTag = text.replace(regexTag, '')

  // regex url
  /* eslint-disable-next-line no-useless-escape */
  const regexUrl = /(http|ftp|https|ftps|Http|Https)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g

  // convert link
  const textConvert = removeTag.replace(regexUrl, function(findURL) {
    return (
      '<a href="' +
      findURL +
      '"' +
      'target="_blank"' +
      'rel="noopener"' +
      '>' +
      findURL +
      '</a>'
    )
  })
  return textConvert
}

/**
 * Function to format currency
 *
 * @param {*}
 * date
 *
 * @returns String
 */
export const currencyFormatter = text => {
  return Math.trunc(text)
    .toString()
    .replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1.')
}

/**
 * Function to format number to string for show purpose after calculation
 *
 * @param {*}
 * date
 *
 * @returns String
 */
export const numberToString = text => {
  return Math.trunc(text)
    .toString()
    .replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1.')
}

/**
 * Function to format date (without time detail)
 *
 * @param {*}
 * date
 *
 * @returns
 */
export const formatDate = date => {
  const d = new Date(date)
  let month = '' + (d.getMonth() + 1)
  let day = '' + d.getDate()
  const year = d.getFullYear()
  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day
  return [day, month, year].join('-')
}

/**
 * Function to format date (with time detail)
 *
 * @param {*}
 *
 * @returns
 */
export const formatDateTime = date => {
  const months = [
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  ]
  const d = new Date(date)
  let month = '' + months[d.getMonth()]
  let day = '' + d.getDate()
  let hour = '' + d.getHours()
  let minute = '' + d.getMinutes()
  const year = d.getFullYear()
  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day
  if (hour.length < 2) hour = '0' + hour
  if (minute.length < 2) minute = '0' + minute
  return [day, ' ', month, ' ', year, ' - ', hour, ':', minute].join('')
}

/**
 * Function to count refund on some order detail pages
 *
 * @param {*} { oriQty, sellerQty, unitPrice }
 *
 * @returns number
 */
export const countRefund = (oriQty, sellerQty, unitPrice) => {
  return (Math.trunc(oriQty) - Math.trunc(sellerQty)) * Math.trunc(unitPrice)
}

/*
 * function getDiffSecondTime : use to get difference between 2 datetime & format it in seconds
 * @param timeToCompare     	date UTC Format   		required (future time)
 * @param compareTime     		date UTC Format   		required (default NOW())
 * return						          int	(seconds)
 */
export const _getDiffSecondTime = (timeToCompare, compareTime) => {
  const isSafari =
    navigator.vendor &&
    navigator.vendor.indexOf('Apple') > -1 &&
    navigator.userAgent &&
    navigator.userAgent.indexOf('CriOS') === -1 &&
    navigator.userAgent.indexOf('FxiOS') === -1
  let a = new Date()
  if (typeof compareTime !== 'undefined') {
    if (isSafari && typeof compareTime !== 'undefined') {
      compareTime = compareTime.toString().replace(/-/g, '/') // replace 'YYYY-MM-DD' to 'YYYY/MM/DD' if in safari
    }
    a = new Date(compareTime)
  }
  if (isSafari && typeof timeToCompare !== 'undefined') {
    timeToCompare = timeToCompare.toString().replace(/-/g, '/') // replace 'YYYY-MM-DD' to 'YYYY/MM/DD' if in safari
  }
  const b = new Date(timeToCompare)
  const difference = _roundNumber((b - a) / 1000, 0)
  return difference
}

// This is additional for above helper (diff)
export const _roundNumber = (value, ndec) => {
  let n = 10
  for (let i = 1; i < ndec; i++) {
    n *= 10
  }

  if (!ndec || ndec <= 0) return Math.round(value)
  return Math.round(value * n) / n
}
